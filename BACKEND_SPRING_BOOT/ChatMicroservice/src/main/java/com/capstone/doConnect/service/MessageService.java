package com.capstone.doConnect.service;

import java.util.List;
import javax.validation.Valid;

import com.capstone.doConnect.dto.MessageDTO;


public interface MessageService
{
	public MessageDTO sendMessage(@Valid MessageDTO messageDTO);
	public List<MessageDTO> getMessage();
}
