package com.wipro.doconnect.dto;

import javax.validation.constraints.*;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AskQuestionDTO
{
	@NotNull(message = "Please Provide the ID")
	private Long userId;
	
	@NotBlank(message = "Question required")
	private String question;
	
	@NotBlank(message = "Please provide the topic")
	private String topic;

	
}
