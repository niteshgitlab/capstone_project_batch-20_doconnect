package com.wipro.doconnect.dto;

import javax.validation.constraints.*;
import lombok.*;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message
{
	@NotBlank(message = "Provide User Details")
	private String fromUser;
	
	@NotBlank(message = "Provide Message")
	private String message;

	
}
