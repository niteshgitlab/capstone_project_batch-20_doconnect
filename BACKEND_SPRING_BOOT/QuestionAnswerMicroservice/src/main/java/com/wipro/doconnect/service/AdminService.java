package com.wipro.doconnect.service;

import java.util.List;
import com.wipro.doconnect.dto.ResponseDTO;
import com.wipro.doconnect.entity.Answer;
import com.wipro.doconnect.entity.Question;

public interface AdminService
{
	
	
	public Question approveQuestion(Long questionId);
	public Answer approveAnswer(Long answerId);
	
	public List<Question> getUnApprovedQuestions();
	public List<Answer> getUnApprovedAnswers();
	
	public ResponseDTO deleteQuestion(Long questionId);
	public ResponseDTO deleteAnswer(Long answerId);

}
