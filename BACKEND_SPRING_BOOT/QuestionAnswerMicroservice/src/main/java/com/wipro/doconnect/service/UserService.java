package com.wipro.doconnect.service;

import java.util.List;
import javax.validation.Valid;
import com.wipro.doconnect.dto.AskQuestionDTO;
import com.wipro.doconnect.dto.Message;
import com.wipro.doconnect.dto.PostAnswerDTO;
import com.wipro.doconnect.entity.Answer;
import com.wipro.doconnect.entity.Question;

public interface UserService
{
	
	
	public Question askQuestion(@Valid AskQuestionDTO askQuestionDTO);
	public Answer giveAnswer(@Valid PostAnswerDTO postAnswerDTO);
	
	public List<Question> searchQuestion(String question);
	public List<Answer> getAnswers(Long questionId);
	public List<Question> getQuestions(String topic);
	
	public Message sendMessage(@Valid Message message);
}
