package com.wipro.doconnect.serviceImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wipro.doconnect.dto.ResponseDTO;
import com.wipro.doconnect.entity.Answer;
import com.wipro.doconnect.entity.Question;
import com.wipro.doconnect.exception.NotFound;
import com.wipro.doconnect.repository.AnswerRepository;
import com.wipro.doconnect.repository.QuestionRepository;
import com.wipro.doconnect.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService
{
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private AnswerRepository answerRepository;

	
	@Override
	public List<Question> getUnApprovedQuestions()
	{
		return questionRepository.findByIsApproved();
	}

	@Override
	public List<Answer> getUnApprovedAnswers()
	{
		return answerRepository.findByIsApproved();
	}

	@Override
	public Question approveQuestion(Long questionId)
	{
		Question question = questionRepository.findById(questionId).orElseThrow(() -> new NotFound("Question not found"));

		question.setIsApproved(true);
		question = questionRepository.save(question);
		return question;
	}

	@Override
	public Answer approveAnswer(Long answerId)
	{
		Answer answer = answerRepository.findById(answerId).orElseThrow(() -> new NotFound("Answer not found"));

		answer.setIsApproved(true);
		answer = answerRepository.save(answer);
		return answer;
	}

	@Override
	public ResponseDTO deleteQuestion(Long questionId)
	{
		ResponseDTO responseDTO = new ResponseDTO();
		Question question = questionRepository.findById(questionId).orElseThrow(() -> new NotFound("Question not found"));

		questionRepository.delete(question);
		responseDTO.setMsg("Question removed");
		return responseDTO;
	}

	@Override
	public ResponseDTO deleteAnswer(Long answerId)
	{
		ResponseDTO responseDTO = new ResponseDTO();

		Answer answer = answerRepository.findById(answerId).orElseThrow(() -> new NotFound("Answer not found"));

		answerRepository.delete(answer);
		responseDTO.setMsg("Answer Removed");
		return responseDTO;
	}
}
