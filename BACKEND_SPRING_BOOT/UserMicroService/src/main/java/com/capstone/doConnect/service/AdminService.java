package com.capstone.doConnect.service;

import java.util.List;

import com.capstone.doConnect.DTO.DeleteUserDTO;
import com.capstone.doConnect.entity.Admin;
import com.capstone.doConnect.entity.User;

public interface AdminService
{
	public Admin adminLogin(String email, String password);
	public String adminLogout(Long adminId);
	public Admin adminRegister(Admin admin);
	
	public User getUser(String email);
	public List<User> getAllUser();
	public DeleteUserDTO deleteUser(Long answerId);
}
