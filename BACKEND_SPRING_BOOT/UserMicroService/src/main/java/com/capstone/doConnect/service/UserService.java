package com.capstone.doConnect.service;

import javax.validation.Valid;

import com.capstone.doConnect.entity.User;

public interface UserService
{
	public User userLogin(String email, String password);
	public String userLogout(Long userId);
	public User userRegister(@Valid User user);
}
