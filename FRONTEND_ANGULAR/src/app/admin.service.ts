import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Answer } from './answer';
import { Question } from './question';
import { User } from './user';
import { Admin } from './admin';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http:HttpClient) {

   }

   admin= new Admin()

   url1:string="http://localhost:9091/admin"
   url2:string="http://localhost:9092/admin"

   sendAdminData(admin:Admin){
    console.log("admin data coming from "+admin.id)
    this.admin.id=admin.id
    this.admin.email=admin.email
    this.admin.name=admin.name
   }

   giveAdminData(){
    return this.admin
   }

   adminLogin(email:string,password:string):Observable<Admin>{
    return this.http.get<Admin>(this.url1+"/login/"+email+"/"+password)
   }

   adminLogout(adminId:number):Observable<string>{
    return this.http.get<string>(this.url1+"/logout/"+adminId)
   }

   adminRegister(admin:Admin):Observable<Admin>{
    return this.http.post<Admin>(this.url1+"/register",admin)
   }

   getUnApprovedQuestions():Observable<Question[]>{
     return this.http.get<Question[]>(this.url2+"/getUnApprovedQuestions")
   }
   getUnApprovedAnswers():Observable<Answer[]>{
     return this.http.get<Answer[]>(this.url2+"/getUnApprovedAnswers")
   }

   question={
  }
   approveQuestion(questionId:number):Observable<Question>{
     return this.http.put<Question>(this.url2+"/approveQuestion/"+questionId,this.question)
   }

   approveAnswer(answerId:number):Observable<Answer>{
     return this.http.put<Answer>(this.url2+"/approveAnswer/"+answerId,Answer)
   }
   deleteQuestion(questionId:number){
     return this.http.delete(this.url2+"/deleteQuestion/"+questionId)
   }
   deleteAnswer(answerId:number){
     return this.http.delete(this.url2+"/deleteAnswer/"+answerId)

   }
   getUser(email:string):Observable<User> {
     return this.http.get<User>(this.url1+"/getUser/"+email);
   }

   deleteUser(userId:number){
    return this.http.delete(this.url1+"/deleteUser/"+userId)
   }

   getAllUsers():Observable<User[]>{
    return this.http.get<User[]>(this.url1+"/getAllUsers")
   }

}
