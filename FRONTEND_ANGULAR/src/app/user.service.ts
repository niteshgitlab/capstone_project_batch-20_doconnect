import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable } from 'rxjs';
import { Question } from './question';
import { Answer } from './answer';


@Injectable({
	providedIn: 'root'
})
export class UserService {

	constructor(private http: HttpClient) {

	}

	questionId: number = 0
	user = new User()
	url1: string = "http://localhost:9091/user"
	url2: string = "http://localhost:9092/user"
	
	sendUserData(user: User) {
		this.user.id = user.id
		this.user.name = user.name
		this.user.email = user.email
	}

	giveUserData() {
		return this.user
	}

	getQuestionId(questionId: number) {
		this.questionId = questionId
	}
	sendQuestionId() {
		return this.questionId

	}

	userLogin(email: string, password: string): Observable<User> {
		return this.http.get<User>(this.url1 + "/login/" + email + "/" + password);
	}

	userLogout(userId: number) {
		return this.http.get(this.url1 + "/logout/" + userId);
	}

	userRegister(user: User): Observable<User> {
		return this.http.post<User>(this.url1 + "/register", user)
	}

	askQuestion(askQuestionDTO: any): Observable<Question> {
		return this.http.post<Question>(this.url2 + "/askQuestion", askQuestionDTO);
	}

	giveAnswer(postAnswerDTO: any): Observable<Answer> {
		return this.http.post<Answer>(this.url2 + "/giveAnswer", postAnswerDTO);
	}

	searchQuestion(question: string): Observable<Question[]> {
		return this.http.get<Question[]>(this.url2 + "/searchQuestion/" + question);
	}

	getAnswers(questionId: number): Observable<Answer[]> {
		return this.http.get<Answer[]>(this.url2 + "/getAnswers/" + questionId);
	}

	getQuestions(topic: string): Observable<Question[]> {
		return this.http.get<Question[]>(this.url2 + "/getQuestions/" + topic);
	}
}
